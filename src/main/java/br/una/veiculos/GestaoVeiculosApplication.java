package br.una.veiculos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoVeiculosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoVeiculosApplication.class, args);
	}
}
