package br.una.veiculos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.una.veiculos.model.Veiculo;
import br.una.veiculos.model.Veiculos;

@Controller
@RequestMapping("/veiculos")
public class VeiculosController {

	@Autowired
	private Veiculos veiculos;
	
	@GetMapping
	public ModelAndView listar1() {
		ModelAndView modelAndView = new ModelAndView("ListaVeiculos");
		modelAndView.addObject("veiculos", veiculos.findAll());
		modelAndView.addObject(new Veiculo());
		return modelAndView;
	}
	
	@PostMapping
	public String salvar(Veiculo veiculo) {
		this.veiculos.save(veiculo);
		return "redirect:/veiculos";
	}
}
